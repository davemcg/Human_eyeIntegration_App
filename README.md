# Human eyeIntegration Alpha Web App Source Code

![](www/quick_overview.mov)

# Mission
An important tool in understanding basic biology and unravelling the causes of disease has been the analysis of gene expression profiles. The [GTEx Project](http://www.gtexportal.org) has been used to great effect to identify genetic variation altering gene expression and gene networks to identify co-regulated genes across dozens of different tissues. They also share this data via a powerful and easy-to-use web portal. However, the eye was not included as a tissue for this project. 

The goal of this project is to provide a high quality and consistenty processed source of all publical available human eye RNA expression data-sets. This makes possible easy comparison of gene expression patterning across across different tissue of the eye. To compare against other tissues, gene expression data from the [GTEx Project](http://www.gtexportal.org) was brought in. Parwise differential expression of nine classes of eye tissues (cornea, RPE, and retina across immortalized and stem cell lines, fetal and adult tissue) were computed against each other and human embryonic stem cells and pseudo-human 'body' set of evenly mixed samples across the tissues from the GTEx project. The RPE and retina samples were large enough to create [WGCNA](https://labs.genetics.ucla.edu/horvath/CoexpressionNetwork/Rpackages/WGCNA/) networks to study interacting genes. 

# Brief Methods
The raw sequencing reads were obtained and processed with an identical bioinformatic workflow. Extensive QC was doneto remove samples with both lower sequencing quality and unexpected behavior during undirected clustering. Read scoring was scaled for transcript size, library depth, and quantile normalized by tissue type. Transcript level quantitation 
was merged to the gene level to improve power and lower variance. Finally genes with low expression across all tissues were removed from analysis.

Differential expression was done by [limma](https://academic.oup.com/nar/article/43/7/e47/2414268/limma-powers-differential-expression-analyses-for) with [voom](https://genomebiology.biomedcentral.com/articles/10.1186/gb-2014-15-2-r29) correction with the expression values calculated above.

Gene Correlation networks were constructed with all available Retina and RPE samples with WGCNA following their tutorial.

# Web Site
This project is hosted on the web at https://eyeIntegration.nei.nih.gov


# Procedure for local install of this application
1. Install R
 - https://www.r-project.org
2. Install RStudio Desktop (Open Source License)
 - https://www.rstudio.com/products/rstudio/download/
3. Open RStudio, click on 'File - R Script' and paste the following into 'Untitled1':
```
install.packages('plotly')
install.packages('shiny')
install.packages('shinyjs')
install.packages('ggplot2')
install.packages('dplyr')
install.packages('tidyr')
install.packages('broom')
install.packages('DT')
install.packages('ggthemes')
install.packages('Hmisc')
install.packages('visNetwork')
install.packages('colourpicker')
install.packages('igraph')
install.packages('RSQLite')
install.packages('ggiraph')
source("https://bioconductor.org/biocLite.R")
biocLite("limma")
```
 - Then select all and click on the 'Run' icon on the top right of Untitled1 
 - ![](run_commands.png)
4. `git clone` this project, `cd` into it, then run `git lfs pull`
 - `git lfs` may need to be installed: https://git-lfs.github.com
 - Go to step 6
5. If you don't understand what the above means (step 4), then download the source via this link: 
 - http://helix.nih.gov/~mcgaugheyd/eyeIntegration_2018_05_07.zip
 - THIS WILL LIKELY BE SEVERAL UPDATES BEHIND THIS REPOSITORY. IF YOU KNOW HOW TO `git`, DO THAT
 - Then unzip the file
6. If you used git to clone this project, download the two sql database for the 1. gene expression and network data and 2. single cell data and place in the www folder
 - http://helix.nih.gov/~mcgaugheyd/eyeIntegration_human.sqlite
 - http://helix.nih.gov/~mcgaugheyd/single_cell_retina_info_2.sqlite
7. In RStudio click on File - Open File, then navigate to the cloned/downloaded project and open up 'server.R'
8. Click on the 'Run App' icon in the top right of the server.R code window
 - ![](run_app_button.png)
 - The app should initialize well within 20 seconds