---
title: "eyeIntegration Viewing Stats"
output: html_notebook
---

```{r}
# https://heuristically.wordpress.com/2013/05/20/geolocate-ip-addresses-in-r/
freegeoip <- function(ip, format = ifelse(length(ip)==1,'list','dataframe'))
{
  if (1 == length(ip))
  {
    # a single IP address
    require(rjson)
    url <- paste(c("http://freegeoip.net/json/", ip), collapse='')
    ret <- fromJSON(readLines(url, warn=FALSE))
    if (format == 'dataframe')
      ret <- data.frame(t(unlist(ret)))
    return(ret)
  } else {
    ret <- data.frame()
    for (i in 1:length(ip))
    {
      r <- freegeoip(ip[i], format="dataframe")
      ret <- rbind(ret, r)
    }
    return(ret)
  }
}   

```

Pull in IP addresses, filter to only keep IP addresses over 11 views (I think any 11 or below is a bot, since they are so common)

`eyeIntegration.stats` is generated as follows:

1. Select all `Daily_eyeinegration_frequent_IP_access_list` emails in Outlook
2. Drag all emails to empty folder
3. Run `cat *eml | grep "^[[:digit:]]" > eyeIntegration.stats`

```{r}
library(tidyverse)
library(rworldmap)
ipaddresses <- read_delim('~/Desktop/untitled folder/eyeIntegration.stats', col_names = F, delim = '\\s+') %>% 
  separate(col = X1, into = c('IP Address', 'Page Views'), sep='\\s+') %>% 
  mutate(`Page Views` = as.numeric(`Page Views`)) %>%  
  filter(`Page Views` > 11)
```

Total page view from May 2017 to November 2017
```{r}
sum(ipaddresses$`Page Views`)
```

Get latitude and longitude for all unique IPs (takes a few minutes to query)
```{r}
unique_IP <- ipaddresses$`IP Address` %>% unique()
ip_info <- list()
for (i in unique_IP){
  ip_info[[i]] <- freegeoip(i)
}
```

Process and clean up
```{r}
ips <- ''
latitudes <- ''
longitudes <- ''
cities <- ''
countries <- ''
for (i in unique_IP){
  ips <- c(ips, ip_info[[i]]$ip)
  latitudes <- c(latitudes, ip_info[[i]]$latitude)
  longitudes <- c(longitudes, ip_info[[i]]$longitude)
  cities <- c(cities, ip_info[[i]]$city)
  countries <- c(countries, ip_info[[i]]$country_name)
}

geo_locate <- cbind(ips, latitudes, longitudes, cities, countries) %>% 
  data.frame() %>% 
  mutate(latitudes = as.numeric(as.character(latitudes)), longitudes = as.numeric(as.character(longitudes))) %>% 
  filter(abs(latitudes) > 0) #remove empty coordinates
```

Interactive Map
```{r}
library(ggiraph)
world <- map_data("world")
world <- world[world$region != "Antarctica",]

#world <- c(geom_polygon(aes(long,lat,group=group), size = 0.1, colour= "#090D2A", fill="white", alpha=0.8, data=worldmap))
code <- ggplot() + geom_map(data=world, map=world,
                    aes(x=long, y=lat, map_id=region),
                    color="#090D2A", fill="white", size=0.05, alpha=1/4) + 
  geom_point_interactive(aes(x = geo_locate$longitudes, y = geo_locate$latitudes, tooltip=geo_locate$cities)) + theme_void() 
ggiraph(code = print(code) )
```

Regular Map
```{r}
ggplot() + geom_map(data=world, map=world,
                    aes(x=long, y=lat, map_id=region),
                    color="#090D2A", fill="lightgray", size=0.05, alpha=1/4) + 
  geom_point(aes(x = geo_locate$longitudes, y = geo_locate$latitudes, tooltip=geo_locate$cities)) + theme_void() 

```
